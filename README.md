### PHP Example APP

> Taken from [this source](https://www.tutorialrepublic.com/php-tutorial/php-mysql-crud-application.php) and modified for demo purpose.

> Only Index and Create Implemented. to Lazy to implement Update and Delete.

> Database of [laravel-example-1](https://gitlab.com/PrijalBista/laravel-example-1) used for this project, if u want to create another test db, heres sql to create the table

```sql
CREATE table posts (
    id int AUTO_INCREMENT PRIMARY KEY,
    title varchar(255),
    description text,
    created_at timestamp,
    updated_at timestamp
);
```

### About the project

This simple application demonstrates conventional approach of writing a php application.
